Il router com’è fatto? 

ha delle porte in ingresso e in uscita. Di fatto nella realtà ogni porta è in ingresso e in uscita nello stesso momento, ma può essere usata una via per volta. Il cuore del router è lo **switching fabric** che è l’oggetto che permette ai pacchetti di essere forwardato verso le altre porte. Il **routing processor** è l’oggetto che svolge gli algoritmi di instradamento per costruire i percorsi, e attraverso cui poi si programma il router processor e quindi si rende possibile l’inoltro dei pacchetti sulla rete. Una volta definiti i percorsi il routing processor scriverà nelle tabelle di farwarding per permettere di smistare il traffico in ingresso e in uscita.

L’instradamento viene fatto attraverso il routing processor e l’inoltro attraverso lo switching fabric.

![image-20211211190010895](image/image-20211211190010895.png)

La porta di ingresso di base ha 3 fasi, che partono da livello fisico e arriva fino a livello 3. Si passa praticamente dalla terminazione della linea, ovvero il livello fisico. Questa sezione converte il segnale in una sequenza di bit/byte. Il livello 2 permette usando la sua specifica, identifica il frame e lo deincapsula e passa il payload (un dataframe di livello 3) al layer successivo. Il lookup e forwarding *queuing* questa sezione è il buffer in entrata dei dati e si aspetta lo switching fabric, ovvero il cuore della rete che permette capire poi la direzione del pacchetto da mandare.



Questi tre livelli sono effettivamente fisico, accodamento e rete. Di fatto tutti i pacchetti si fermano al livello di rete nel router. 

Il buffer della connessione di rete è pensata in modo da compensare il ritardo di calcolo dello switching fabric nel caso di reti ad alte prestazioni. Di solito viene sempre introdotto per sicurezza.

L’obiettivo del router è avere un elaborazione dello switching fabric alla stessa frequenza con cui i pacchetti arrivano in entrata. Questo significa che il buffer in ingresso è esagerato rispetto alla potenza di calcolo del router. 

Un oggetto hardware in grado di forwardare i pacchetti alla velocità fisica della rete è molto difficile. I modelli più performanti di router è possibile avere un’operazione di switch che può andare a qualche Gbit/sec, fino a circa 100Gbit/sec nei migliori modelli. Il costo di questo hardware cresce esponenzialmente.



### Tre tecniche di commutazione dei pacchetti

La switching fabric di un router è particolare, siccome è quello che diversifica il router da un normale host/computer. 

![image-20211211190122725](image/image-20211211190122725.png)

#### Commutazione in memoria

Si utilizza un architettura general pourpose: per questo motivo i primi router erano semplicemente dei computer più di un interfaccia di rete.

Un modello più facile per fare un router è uno **switching in memoria**. Questo significa che si ha una memoria condivisa. I pacchetti vengono memorizzati in memoria e poi ritrasmessi sulla porta in uscita.

Questo modello è il primo modello mai utilizzato, infatti è molto simile a un computer. 

Questo modello è il più facile, ma anche il meno efficiente: siamo limitati dal bus di sistema e dalla capacità del computer utilizzato. Infatti è necessario prendere il pacchetto dall’interfaccia fisica, spostarlo nel buffer di memoria dell’host, elaborare il pacchetto e poi forwardare nuovamente il pacchetto facendolo passare di nuovo dal bus di sistema. 

![image-20211211190441668](image/image-20211211190441668.png)

* il pacchetto entra nell’host
* viene memorizzato: serve per l’elaborazione (che sia il checksum oppure la semplice lettura dell’header)
* si sposta sulla porta di uscita



In questo modello la frequenza massima dei pacchetti è limitata dalle porte di ingresso e quelle di uscita, con una frequenza totale inferiore a $BUS-Sistema/2$, siccome ci sono due trasferimenti sul bus di sistema per ogni pacchetto scambiato attraverso questa rete.



Siccome questa soluzione non è la migliore si ha cambiato approccio.

#### Commutazione tramite bus

Si prende un archiettura in cui si toglie la memoria e si utilizza il bus come interfaccia tra ingressi e uscite. 

Queste tecnologie si chiamano Accesso diretto in memoria o bus condiviso e permettono lo switching senza la necessità di un accesso in memoria.

Logicamente ogni volta che viene preso il bus il pacchetto sa già quale uscita prendere, in questo modo si trasferisce direttamente da porta in uscita a porta in entrata direttamente.

Di nuovo, la larghezza di banda di commutazione del pacchetto è limitata dal bus, ma c’è già il doppio di banda (preso la stessa applicazione) disponibile, siccome non si ha un doppio utilizzo del bus e la linea è dedicata.

un altro problema **contesa per il bus**: ci potrebbero essere più pacchetti che vogliono accedere al bus nello stesso momento. Per questo motivo è necessario un modello di controllo.

Esempio: il router Cisco 5600 ha questa architettura e ha un bus di 32Gbit/sec e è un router sufficente all’accesso della rete (vicino al bordo della rete), senza necessità di aggregare link sulla rete. Questo bus infatti viene diviso tra le capacità di rete: potrebbe approssimativamente 3 porte da 10Gbit/sec. Dall’altro canto questa architettura riesce a essere abbastanza efficiente per alcuni usecase, ad esempio lo switching sul gigabit al secondo, mantenendo dei costi piuttosto contenuti.



L’unico modo per ottenere maggiori prestazioni si deve cambiare completamente architettura. 



#### Architettura CrossBar

Questo significa che il router può attivare contemporaneamente più connessioni, questo significa che si possono avere delle connessioni che funzionano contemporaneamente, logicamente con una logica hardware ottimizzata in modo molto spinto, in modo da evitare dei blocchi sul bus menre si fanno degli switch sul traffico.

Questo modello viene con un costo non indifferente, infatti è il primo modello che ha un hardware completamente dedicato e è completamente diverso da un computer. Questi router sono dedicati al cuore della rete e sono in grado di gestire Terabit/sec di dati.



Il modello riesce quindi a superare il limite di banda di un singolo bus condiviso.

Riesce a sviluppare una rete d’interconnessione che consiste in $2n$ bus che collegano $n$ porte d’ingresso a $n$ porte di uscita.

Un hardware Switch Cisco 12000 (si chiama Switch perché non è un completo router, fa solo farwarding) e questo modello riesce a fare forwarding fino a 60Gbps. 

Per favorire la gestione del CrossBar e trasmettono in modo da avere dei frammenti a durata fissa. In questo modo, sapendo la durata di ogni task, si riesce a ottimizzare la connessione. Quando un pacchetto di questo tipo entra nel router, poi il pacchetto passa sull’archittettura crossbar in modo completamente **sincrono**, e quindi il flusso dei pacchetti sarà sempre definito da un clock.

Queste soluzioni possono essere molto complicate più diventano performanti. In questo modo si hanno dei controller che permettono di far circolare sul modello più di un pacchetto, mentre invece solitamente si ottiene un solo pacchetto al colpo.

Le altre architetture lavorano su un modello di interrupt.



#### Porta di uscita

La porta di uscita è la versione specchiata di quella di ingresso: quando un pacchetto arriva dall’architettura di smistamento superiore, allora raggiunge u buffer, poi l’incapsulamento nel layer di link e infine la trasmissione e la traduzione in bit del pacchetto.

Il buffer in uscita è necessario siccome l’interfaccia solitamente ha un tempo di accodamento non univoco. Di solito infatti questo è il buffer che soffre nel caso di congestione, infatti se si trasmettono troppi pacchetti rispetto alla velocità di uscita. Questo buffer è quello che deve sopportare l’accodamento sul canale di uscita.

Questo modello può essere accoppiato a uno scheduler, che può non esser FIFO, ma anche decidere alcuni meccanismi differenti sulla gestione dei pacchetti.

in questo buffer è possibile anche buttare via pacchetti. Oppure si possono avere più buffer e avere due tipologie di code e si vuole dare la precedenza a diversi pacchetti. Tutto questo avviene in uno scheduler. 

 Di solito nei router commerciali ci sono una serie di funzionalità di scheduler applicabili (non è necessario programmare completamente la logica), ma di fatto non si può inventare completamente, ma sono dei modelli tra cui scegliere codificati  (solitamente) nel firmware del router.



Un appunto non banale: **la dimensione del buffer**. Ci sono dei lati negativi sia nell’avere un buffer troppo piccolo o troppo grande. 

un buffer troppo piccolo non permette di avere una situazione ottimale nel caso dell’accodamento o in caso di congestione, siccome non sa immagazzinare abbastanza pacchetti per sopportare neanche una congestione lieve.

Dall’altro canto, se il buffer è troppo grande si ottiene un effetto contrario: non si rischierà mai di andare in congestione grave, ma a questo punto si perde la capacità di controllare il ritardo. infatti i pacchetti all’aumentare del carico i pacchetti saranno spesso in attesa, cosa che può far aumentare di molto la rete. 

La dimensione raccomandata per la dimensione del buffer viene per la prima volta affrontato nel `RFC 3439`. Questo RFC indica come dimensionare il buffer: la quantità di memoria associata al buffer dovrebbe essere una media del RTT per la capacità di collegamento. Per questo motivo servono simulatori o dati per capire le dimensioni necessarie al buffer. 	 

Questo modello inizia a diventare molto grande, soprattutto sui router più grandi. 

Attualmente si raccomanda sia:
$$
\frac{RTT * C}{\sqrt{N}}
$$
Questa formula è abbastanza empirica, perché il $\sqrt{N}$ non ha fondamenti pratici. Di fatto questo modello permette di non arrivare mai al limite. Questo backbone permette di ridurre di molto la necessità di buffering. La memoria del router costa molto siccome deve essere affidabile e molto veloce, e questa formula permette di ridurre un pochino la necessità di memoria necessaria per il router. 

Spesso si fa l’operazione $RTT*C$ perché rappresenta il volume del canale. Si pensi a un tubo di acqua: la capacità è la base e la lunghezza è il RTT. In questo modo si ottiene una formula spannometrica abbastanza efficace per capire la massima capacità del collegamento, come se tutto il tubo fosse completamente pieno.



## 4.4 Protocollo Internet (IP)

IP sta per Internet Protocol. Non è un protocollo difficile, siccome si basa su una tecnica a commutazione di datagrammi che non è una tecnica difficile, ma di fatto è uno di quei protocolli che viene utilizzato da tutti, compresi chi non sono completamente nel settore delle reti.

Questo protocollo indica:

* le convenzioni sugli indirizzi

* i datagrammi di comunicazione

* delle regole di instradamento e inoltro

* Questo modello inoltre comprende anche la programmazione della tabella di inoltro attraverso i protocolli di instradamento. 

* Protocollo di segnalazione ICMP (la rete a datagrammi non si presta ad avere un protocollo di controllo molto richiesto, ma di fatto servono solo un paio di valori di controllo)

  Esempio: un pacchetto ICMP è ad esempio TraceRouter, Ping

Il livello di rete è dominato da IP, ma ci sono dentro anche queste altre cose. 



### Formato dei datagrammi

Sono sempre 20byte, ovvero 5 righe di intestazione da 32bit. Questo valore di dimensione di header è uguale a quello del TCP. 

Ci sono poi dei campi opzionali, siccome non molto utilizzati, ad esempio si può segnare la registrazione del percorso del pacchetto, dei timestamp (modalità per valutare il RTT), o un elenco di router. Poi segue il payload. Si allineano le intestazioni con una base di 4 byte. 

![image-20211212122727320](image/image-20211212122727320.png)

Prima riga: 

* numero di versione (4 o 6) => il router deve capire subito se è gestibile da lui o meno
* lunghezza intestazione: indica il numero delle righe dell’intestazione, solitamente 5
* tipo di servizio: questo campo è stato studiato, ma mai utilizzato. Su internet non si utilizza, ma viene utilizzato su alcune reti particolari
* lunghezza del datagramma:  16bit => massimo molto grande, di solito 1500byte. Se il pacchetto è troppo grande si può ottenere un rifiuto dal livello di collegamento, e quindi si sceglie una misura che si sa possa viaggiare ovunque.

Seconda riga: (Se il pacchetto è troppo grande allora va spezzettato, e quindi la seconda riga si hanno tutti i parametri per romperlo in maniera opportuna.)

* identificatore a 16 bit
* flag
* spiazzamento di framm. a 13bit.

Terza riga:

* tempo di vita: dipende dal sistema operativo, ma di solito è settato a 64. Questo valore viene decrementato ogni volta che viene elaborato dal router. 
* Protocollo di livello superiore: questo campo indica il protocollo di livello superiore che ha inserito i dati nel payload. Questo è importante per capire a chi consegnare i dati (se UDP o TCP, oppure a ICMP ) oppure alcuni pacchetti di instradamento sono su TCP e indicano che sono pacchetti di instradamento. 
* checksum: IP protegge solo l’intestazione, quindi 5 righe + campi opzionali

4/5 riga:

campi di intestazione per origine (chi ha creato il datagramma) e destinazione (chi lo ha ricevuto) => (32bit). Di questo argomento si parlerà molto perché:

* standard di internet
* bisogna capire come leggere questi indirizzi

Altro: campi opzionali.



### Frammentazione dei datagrammi IP

Quando passate un datagramma da un host a un altro sulla rete ci sono dei limiti massimi di dati mandabili (MTU: maximum transport unit, massima dimensione del pacchetto). Il problema è che in alcune reti questo limite è molto piccolo (Esempio: rete ATM, 50byte). Se io trasmetto un pacchetto di dimensione arbitraria ho due scenari:

* la sorgente sa già che il percorso ha un certo MTU. In questo modo imposto un pacchetto per avere una grandezza massima definita in un certo modo e faccio in modo di non superare mai questo limite
* lascio al livello di rete di frammentare il mio datagramma

Nel secondo caso si fa un operazione automatica al livello di collegamento. I router possono fare questa operazione automaticamente, siccome conoscono gli MTU delle reti che collegano.

La cosa interessante è che i datagrammi nonostante siano dei frammenti, rimangono dei datagrammi validi. In questo modo si creano solo più copie di un datagramma, tutte contenenti un pezzo del payload. I frammenti si possono anche riframmentare, ma rimangono validi, in modo che il router possa riframmentare e ricomporre i frammenti.

Se si vuole trasmettere si ottengono vari pezzettini che vengono spezzettati, poi a destinazione vengono ricomposti. Si utilizza la tecnica di utilizzare datagrammi sempre validi siccome non è detto che i pacchetti siano sempre validi sulla rete e comunque tutto possono avere diversi percorsi sulla rete e devono sempre essere validi per questo motivo. Inoltre nel caso si perda un datagramma di questo genere, allora si ha perso tutto un datagramma originale. La frammentazione ha questo rischio che si aumenta la probabilità di perdere dei pacchetti, siccome un solo frammento annulla la correttezza di tutti il pacchetto originale.



Come funziona questo passaggio:

abbiamo un datagramma di 4000byte su una rete con un MTU=1500byte. Questo pacchetto ha l’intestazione originale così definita: 

```html
Lunghezza = 4000
Flag = 0
Spiazzamento = 0
```

 dove l’identificatore sono i primi 16bit della seconda riga, mentre lunghezza, flag e spiazzamento sono tra la prima e la seconda riga dell’intestazione.

Per forza di cose si ha una lunghezza massima di 1500byte. L’idea di fondo quindi è riempire ogni pacchetto il più possibile e lasciare poi il rimanente nell’ultimo.

Tutti i pacchetti creati hanno una serie di caratteristiche:

* hanno una lunghezza (di solito uguale a MTU per tuttti eccetto l’ultimo pacchetto)
* Flag (settata a uno eccetto che per l’ultimo pacchetto, che viene settato a zero per indicare che è l’ultimo pacchetto)
  * Se il pacchetto deve essere frammentato ulteriormente allora l’ultimo flag di questa ulteriore frammentazione sarà 1, mentre gli altri 2 o  più. 
  * Di fatto è necessario che in ogni operazione di frammentazione si ottenga l’ultimo frammento con valore 0, che indica che si può ricostruire.
  * Questi casi avvengono quando si passa su una serie di router che sono via via più piccoli in MTU e così facendo si ottengono dei valori di Flag di frammentazione sempre maggiori.
*  Spiazzamento:
  * indica il valore del pacchetto in modo da permettere di riordinare tutti i frammenti. 
  * per risparmiare dei byte di comunicazione di questo valore si utilizza la divisione per 8
    * se io ho 1480 byte nel campo dati (20byte di header IP), allora lo spiazzamento viene calcolato con$1480/8$. 
    * Questo valore si ottiene come puntatore, dividendo per 8 la dimensione originale
    * la prima sezione ha lunghezza: $20 H + 1480 P$. Spiazzamento = $0$
    * la seconda sezione ha lunghezza: $20 H + 1480 P$. Spiazzamento = $ 1480/8 = 185$
    * la terza sezione ha lunghezza: $20H + (3980 - 2960)P = 1040 byte$. Spiazzamento $2960/8 = 370$

Solitamente non si frammenta facilmente in questi casi, siccome anche TCP pacchettizza comunque a 1500 byte, che è il MTU molto più frequente.

Di fatto questo non sarebbe necessario se il livello di trasporto crei dei pacchetti più piccoli.

Nella versione 6 del protocollo IP viene tolta completamente. La frammentazione dei pacchetti in questo protocollo avviene solamente nel livello di trasporto, mai nel livello di rete. In questo modo si risolve meno tempo richiesto al router per questa operazione, ma si risponde con un nuovo messaggio di risposta. 

Inoltre avendo così tanti frammenti i pacchetti sono molto più lunghi da mandare e si introduce anche un certo costo da entrambe le parti (mittente e ricevente) per la frammentizzazione e la ricomposizione.



#### Indirizzamento IPv4

L’indirizzo IP è un valore globalmente univoco e viene associata a qualsiasi interfaccia (sia host che router) che sono visibili in rete. 

Esempio di assegnazione di indirizzo:

<img src="image/image-20211213010421635.png" alt="image-20211213010421635" style="zoom:50%;" />

La presenza del router di fatto spezza la rete in sottoreti. Di fatto la presenza dell’interfaccia del router impone la creazione di sottoreti. Il router opera a livello 3, quindi spezza la rete in sottoreti. Ogni interfaccia del router opera su una rete diversa. La rete in alto a sinistra utilizza un algoritmo di indirizzamento che permette di utilizzare i primi tre byte per capire la sottorete.

Di base si può vedere come i primi bit sono uguali per tutti, e solitamente sono uguali anche per il router, mentre invece gli ultimi bit servono a distinguere i vari host.

Questo modello viene applicato in modo da capire quali host sono nella sottorete e quali no. In un momento che voglio distribuire un pacchetto sulla rete, allora devo capire se è nella mia stessa rete oppure se è su un’altra rete. Nel momento in cui voglio trasmettere il pacchetto a un altro host devo sapere se l’host si trova nella mia stessa rete locale, oppure se ad esempio è un’altra sottorete. La differenza tra queste cose è che se io sono nella stessa sottorete non ho necessità del router, mentre invece se io devo uscire dalla mia rete allora è necessario un router. In questo caso è necessario il livello 2, ovvero si incapsula il pacchetto in uno di livello 2, e quindi riesco poi a mandare dei dati. 

Se il destinatario ha dei byte verso destra diversi, allora è necessario uscire dalla sottorete e quindi è necessario il router. 



La parte di sottorete viene fatta attraverso l’indirizzo IP (ho i primi byte che indicano la rete, gli altri byte indicano l’host). Per prima cosa è necessario imparare che c’è la presenza di più reti (o sottoreti, che di base sono delle reti). 

Per definire indirizzo di una sottorete è necessario utilizzare una scrittura particolare: `192.168.1.1/24`. Questa scrittura indica che i primi 24bit da destra sono l’indicazione di sottorete, mentre gli altri sono a disposizione degli host della rete. 

Se io scrivo `/24` vuol dire che i primi 24bit è la parte di indirizzo condivisa tra tutti gli host definiti della rete. Di fatto tutti i valori in questo campo sono validi, da 1 a 32. Questa notazione quindi è necessaria per segnalare il **campo di indirizzi**. Il modello con `/24` è il più diffuso e anche il più facile da ottenere.

I collegamenti diretti sono a loro volta delle sottoreti, quindi anche i collegamenti tra i router sono concettualmente delle sottoreti, e quindi hanno un campo di indirizzi (solitamente basta avere un indirizzo `/30` tranquillamente). Questo indirizzo è completamente in comune tra i due e il primo bit è la segnalazione di uno e l’altro.

Non si può mai utilizzare il campo di indirizzi con 0, perchè **segnala la rete stessa** né un campo con tutti i bit settati a 1. Questo indirizzo è quello di **broadcast** e per forza di cose non si può mai fare su internet, siccome non si potrebbero raggiungere tutti gli host sulla rete, perché si creerebbero miliardi di pacchetti. Il broadcast si può fare solo nelle sottoreti. Questo è un altro motivo per cui si utilizzano dei router spesso, in modo da *delimitare gli indirizzi di broadcast*. Il dominio di broadcast è necessario in modo da avere una comunicazione su tutta la rete, ma deve sempre essere delimitato in qualche modo. A casa di solito è un router molto meno complicato, e quando riceve una comunicazione in broadcast non permette i pacchetti di uscire dalla rete.

Di solito le reti vengono posizionati ad arte, in modo da spezzare le comunicazioni in broadcast e poi riunirle nel caso con il protocollo IP.

ogni host fa broadcast, i router non possono farlo. Di fatto l’host fa queste comunicazioni senza implicare nessun altra comunicazione, dipende soltanto da che pacchetto è. L’unica cosa che si fa sempre in questa modalità è la richiesta dell’indirizzo IP. Il broadcast è una comunicazione che serve a indicare l’esistenza di un host alla rete (ad esempio la stampante che deve comunicare a tutti gli host la sua esistenza) oppure per raggiungere un host di cui non conosco l’indirizzo, ma di cui invece conosco la sottorete di appartenenza.

Alcuni mezzi di comunicazione (ad esempio il wifi) funzionano comunicando sempre in broadcast, ma in questo caso si sta parlando di un host che decide di comunicare il pacchetto con tutti gli host della rete. Inoltre gli access point funzionano a livello 2, quindi di base permettono semplicemente di arrivare al router più vicino, non hanno alcuna capacità di forwarding o indirizzamento. Servono solo da bridge di comunicazione.



La strategia per assegnare gli indirizzi tra gli host è chiamata **CIDR (Classless InterDomain Routing)**. Questa strategia di assegnazione degli indirizzi IP è fatta in modo da non sapere a priori quale sia la parte dell’host e la parte della rete. In pratica sui 32bit dell’indirizzo di rete, si ottiene un valore di bit, partendo da sinistra che indica quelli che possono indicare l’host, mentre invece i rimanenti sono comuni su tutta la rete. 

Questo tipo di indirizzo prende una forma del genere `a.b.c.d/x` dove `x` è il valore di bit che si riservano per l’host. 

<img src="image/image-20211213181233501.png" alt="image-20211213181233501" style="zoom:50%;" /> 

Questo valore (`x`) non deve per forza essere un multiplo di 8 ma può prendere qualsiasi valore possibile.

In questa sezione si deve fare la divisione tra il concetto di **indirizzo della sottorete** e **netmask della sottorete**. Praticamente l’indirizzo della rete è un valore che contiene i bit relativi alla sottorete costanti, mentre invece varia i bit della parte di host. La maschera di sottorete invece è un indirizzo che rappresenta la stessa cosa, ma si lasciano a `1` i bit della parte di sottorete, mentre invece si lasciano a zero i bit della parte relativa all’host.

Di fatto quindi un indirizzo è indicabile univocamente in due modi, sulla stessa sottorete: 

* maschera della sottorete (esempio: `255.255.254.0`)
* indirizzo della sottorete (esempio: `200.23.16.0/23`)

Questi modelli non si utilizzano mai assieme ma o uno o l’altro.

Questo modello è molto comodo e efficace siccome permette di comprendere subito se il pacchetto va consegnato nella stessa sottorete, oppure se deve uscire.  Inoltre essendo un operazione binaria, si può fare un confronto molto veloce (attraverso un semplice `&` binario). In questo modo si può subito capire se mandare a un host interno alla rete o affidarlo al router perché più lontano.



#### Assegnazione di un indirizzo a un host

L’assegnazione di un indirizzo è sempre ottenibile in modo manuale, ma di solito si utilizza un metodo automatico. 

Generalmente la configurazione manuale dell’IP è utile per i server.

Di solito sulle reti è disponibile il **DHCP (Dynamic Host Configuration Protocol)**. Lo scopo di questo protocollo è di rendere “plug-and-play” l’ingresso in una rete, ovvero rende automatico l’assegnazione di un IP a un host sulla rete.

Questo protocollo è quello che permette di non aver quasi mai da gestire una rete. 

L’obiettivo è quello di consentire a un host di entrare in una rete, siccome l’assegnazione di un indirizzo è la base per ottenere una comunicazione sulla rete. 

Di solito il provider utilizzano spesso degli indirizzi dinamici, perché non si hanno troppi indirizzi. Questo approccio permette un ottimo utilizzo di questo protocollo.

Il principio è molto semplice, sono solo 4 messaggi:

*  l’host invia un messaggio di broadcast chiamato *DCHP discover*
* il server DHCP risponde con un messaggio chiamato *DHCP request* (questo messaggio viene ottenuto solo se si trova un server sulla rete)
* l’host richiede l’indirizzo attraverso un *DHCP request*
* il server conferma l’indirizzo con un *DHCP Ack*



Affinché il funzionamento del protocollo DHCP sia corretto si deve avere il server sulla rete locale. Solitamente nelle case si ottiene in un unico pacchetto NAT, server DHCP e router, con magari anche il router WI-FI.

IL DHCP è diverso dal servizio di NAT. Il DHCP è comodo perché non si deve autoconfigurare l’indirizzo IP. Generalemente il DHCP di una sottorete da un indirizzo privato. Il DHCP può essere anche un server che asssegna indirizzi pubblici, ad esempio i server DHCP dei provider forniscono valori pubblici. Se il server fa parte di una rete allora si assegnano gli indirizzi privati della sottorete. 

Come si fa a collegarsi a una certa rete? Di solito con il cavo ethernet ci si collega, ma di solito richiede un autenticazione dell’utente (noi diamo per scontato che questa azione sia già stata fatta) e che si svolgano le azioni necessarie a ottenere l’indirizzo IP.

Le procedure di seguito sono tutte quelle che avvengono appena ci si è autenticati sulla rete.

<img src="image/image-20211213223124564.png" alt="image-20211213223124564" style="zoom:50%;" />  

* mando il protocollo dall’host (porta 68) al server DHCP (porta 67)
  * questo messaggio viene inviato come fosse un broadcast generalizzato
  * l’unico modo è mandare con tutti 1 in modo che il pacchetto venga consegnato a tutti, e siccome il router non lascerebbe uscire il messaggio dalla sottorete
* si utilizza anche un transaction ID, per distinguere le richieste
* si ha un indirizzo di sorgente, un destinatario e poi le porte di destinatario e sorgente. Dentro il payload poi si ottiene la risposta dell’indirizzo offerto dal server DHCP. Questo è il momento in cui il server rilascia un possibile indirizzo e un tempo di vita (il lifetime viene messo perché quando questo valore scade, allora l’host rifà la richiesta. Di solito in questo caso il DHCP ridà l’indirizzo precedente).
* si conferma la ricezione dell’indirizzo proposto e poi conferma del DHCP.
* una volta ultimata queste operazioni allora si ottiene un indirizzo IP per l’host. Spesso poi nell’ultimo pacchetto vengono date altre informazioni (ad esempio l’indirizzo IP del router per uscire dalla sottorete e la maschera di sottorete)



Come ottenere un blocco di indirizzo IP. 

Ad oggi ogni blocco di indirizzi è affittabile dal provider, siccome tutti gli indirizzi sono occupati, al momento. I provider, finché non è stato proposto l’approccio di avere indirizzi privati, avevano la necessità di assegnare un indirizzo pubblico a ogni utente. Di solito per un utente è abbastanza avere un solo indirizzo, mentre le aziende possono avere la necessità di avere più di un indirizzo.

La compravendita degli indirizzi consiste nel comprare dei bit sull’indirizzo ovvero un bit della rete e tutti i valori sotto quell’indirizzo. 

La divisione degli indirizzi si ottiene in modo molto facile: si vuole dividere in un certo valore di indirizzi, mettiamo 8 ($2^3$). Bisogna allungare la maschera di rete di altri 3bit. Ad esempio si ha una maschera di rete `200.23.16.0/20` sull’ISP. Si vogliono fornire indirizzi a 8 organizzazioni, quindi si divide in 8 parti uguali, aggiungendo 3bit alla maschera di rete: `200.23.16.0/23`, `200.21.18.0/23`, `200.23.20.0/23`, `200.23.22.0/23`, `200.23.24.0/23`, `200.23.26.0/23`, `200.23.28.0/23`, `200.23.30.0/23`  . Di fatto questo genera i nuovi prefissi delle reti. Ognuna di queste reti ha 9bit di indirizzi per contenere tutti gli host, ovvero hanno $2^9 = 512$ indirizzi, a cui però vanno tolti il tutto $0$ e il tutto $1$. Quindi questo significa che ci sono $509$ hosts.

![image-20211213230658945](image/image-20211213230658945.png)

Di fatto nei casi pratici si può avere una netmask più restrittiva con `/30`. Quella con tutti zero e quella con tutti uni è la più restrittiva. Di solito questo tipo di connessioni avviene nelle connessioni tramite cavo, che sono necessari solo 2 indirizzi.



MANCA INDIRIZZAMENTO GERARCHICO PIù PRECISO



C’è un pochino di centralizzazione sulla rete internet, infatto **ICANN** (stessa corporazione che risolve i nuovi nomi di dominio e cose varie) e ha la responsabilità di gestire e vendere gli indirizzi online. Di fatto questo valore non può essere distribuito e quindi è necessario essere centralizzati.



#### Traduzione degli indirizzi di rete (NAT) Network Address Translator 

Questo oggetto è utilizzato in connessione casalinga, per separare la rete aziendale/domestica da quella pubblica. Di fatto significa che da un lato è su internet (da qui partono i pacchetti verso la rete internet e quindi ha un indirizzo IP pubblico) e poi dall’altra parte ha un indirizzo riservato (per definizione infatti non sono privati, ma sono riservati. Erano nati con l’idea di usarli per esperimenti e test.). Possono essere `10.0.0.4` oppure del tipo `192.168.1.1`. Di fatto questi indirizzi poi non sono più stati utilizzati sulla rete pubblica. Lo spazio di indirizzi utilizzabili sulla rete privata è grande quando serve, infatti di solito non c’è nessun problema a aumentare il numero di host sulla rete.

Il NAT fa in modo di convertire i pacchetti in uscita e in ingresso in modo che gli host in rete locale siano raggiungibili attraverso l’IP pubblico del NAT.

Chiaramente questa operazione ha alcuni vantaggi:

* è economico (per farlo funzionare è necessario un unico indirizzo IP). Più di un indirizzo inizia a diventare costoso.

* Si possono cambiare tutti gli indirizzi privati dell’abitazione senza influenzare la rete esterna o senza essere in qualsiasi modo bloccato.

* Posso cambiare l’ISP senza cambiare il resto della rete

* (poi diventato uno dei motivi principali per utilizzare il NAT). I dispositivi dietro il NAT non sono esplicitamente indirizzabili e visibili dal mondo esterno, quindi rende la rete interna decisamente più sicura (di fatto questo significa che i computer interni alla rete non sono direttamente raggiungibili.)

  Esempio: la stampante in rete può accedere a internet, ma di fatto mentre lavora nella casa è disponibile solo alla rete interna, non è visibile all’esterno.



L’implementazione del NAT è piuttosto semplice, ma richiede di rompere la divisione dei layer della pila protocollare.

Infatti affinché il NAT funzioni allora è necessario che il router possa gestire gli indirizzi di trasporto (cambiare le porte degli indirizzi di fatto, cosa che in teoria sarebbe vietata dalla stratificazione protocollare). Per questo motivo e per un altro paio di altri il NAT è stato discusso: di fatto interviene su una situazione molto delicata. Questo è necessario perché: 

* quando un router NAT riceve un datagramma allora il NAT crea un nuovo numero di porta in uscita
* sostituisce l’indirizzo privato dell’host con il proprio indirizzo **WAN (Wide Area Network) - Indirizzo Pubblico** e sostituisce il numero di porta di orgine iniziale con il nuovo numero.

Praticamente quello che viene fatto è che sostituisce l’indirizzo del pacchetto con il proprio indirizzo pubblico (in modo che poi possa transitare sulla rete). Questo però genera un problema: il router NAT deve sapere a chi inoltrare la risposta. Per raggiungere questo scopo si utilizza anche il numero di porta, che serve quindi a identificare l’indirizzo di porta originario.

L’immagine sotto identifica meglio lo scenario:

![image-20211214101834551](image/image-20211214101834551.png)



Di fatto la tabella di traduzione NAT serve a “spiegare” al router dove i pacchetti debbano essere inoltrati e di fatto permette quindi il trasporto dei pacchetti dentro e fuori la rete pubblica.

Il NAT in questo modo permette di avere un numero di indirizzi IP potenzialmente infinito (poi in realtà non così tanto). Inoltre permette di avere un range di IP privati molto grande. Inoltre non ci accorge della sua presenza se non per l’indirizzo privato della connessione. 

Se si è dietro il router NAT per sapere il proprio indirizzo IP pubblico è necessario appoggiarsi a qualche servizio che indica l’indirizzo pubblico presente sulla rete.

Il NAT si è diffuso molto per la sicurezza che fornisce e per il moltiplicatore di indirizzi IP che permette di fare. Il blocco di questo modello è il numero di porta (di fatto impone il numero di host fornibili con questi indirizzi fino al numero di porte del NAT, quindi meno di 65000). 

Questo modello è stato contestato dai puristi della rete, siccome infrange il modello della stratificazione della rete.

Inoltre si parla di **argomento punto-punto**: siccome il NAT deve scrivere nell’intestazione del pacchetto di trasporto, questo viola l’argomento punto-punto, perché di fatto quello che viene fatto dal livello di trasporto in su non dovrebbe avere interferenze della rete. Questo è un rischio siccome se il NAT avesse dei problemi, allora si perderebbero dei pacchetti.

Inoltre questa argomentazione direbbe che passando a IP versione 6 (che il numero di indirizzi è potenzialmente infinito e non esisterebbero queste necessità di moltiplicazione delle connessioni). 

Ci sono altri problemi che non si possono risolvere in maniera molto elegante. Il client fa in modo di creare indirizzamento pubblico-privato. Ma di fatto come si fa se il client è fuori dalla rete e si vuole dare accesso a qualcuno che non è in indirizzo privato. Il client non potrà raggiungere il server dietro il NAT finchè la connessione non è inizializzata dalla rete internamente, siccome nella tabella di traduzione NAT non è presente ancora nessun valore. Questo problema ha alcune soluzioni:

* SOLUZIONE 1: si inserisce la corrispondenza sulla tabella degli indirizzi NAT a mano sul NAT in modo da permettere di accedere sempre dall’esterno.
  * Questo modello ha alcuni problemi: 
    * L’ISP può fornire un router che non permette questa azione
    * si possono avere dei problemi di configurazione e/o sbagliare
* SOLUZIONE 2: Universal Plug and Play (**UPnP**) Internet Gateway Device (IGD) Protocol. 
  * Questa azione permette, quando si accende l’host, di richiedere un qualche tipo di mapping per un qualsiasi tipo di porta. 
  * Questo modello si utilizza per un sacco di oggetti privati. Questo può succedere che le porte siano state assegnate e di solito spegnere e riaccendere il router, e di solito questo funziona siccome fa resettare la tabella NAT e quindi in questo modo gli oggetti smart possono richiedere di nuovo la porta e prenotarsela, in modo da avere l’accesso a internet. 
  * Questo si utilizza spesso per i dispositivi che sono oggetti (non computer), siccome è una soluzione abbastanza elegante e è plug-and-play.
* SOLUZIONE 3: questa è una soluzione estrema e di fatto si utilizza quando le altre non funzionano o non sono applicabili. Questa viene fatto quando ho un software dietro il NAT allora si utilizza una connessione a un nodo esterno che permetta di mantenere la entry sul NAT, poi rimango connesso con il super-nodo e al quando vengo ricercato allora il relay si occupa di connettermi con l’host attraverso la connessione impostata tramite NAT. Questo funziona siccome finché non si fa traffico sulla rete allora non si crea la entry dietro al NAT. Se io devo utilizzare un software e so che verrà spesso utilizzato dietro il NAT allora devo trovare subito una soluzione.
  * Questa soluzione è quella utilizzata da Skype (questa funziona perché il servizio ha dei nodi divisi abbastanza bene su tutta la popolazione)
  * Spesso il telefono VoIP (Voice Over IP) utilizza questo modello: la gente per chiamare deve utilizzare una tecnica di questo tipo. In questo modello di solito si mette uno scatolotto sull’internet pubblica (come fosse un centralino). Di solito chi vende il servizio VoIP vende questo scatolotto che permette di essere esterno al NAT e mantenere la connessione.



### ICMP (Internet Control Message Protocol)

Ad esempio questo servizio viene fatto ad esempio con il ping oppure quando i router avvisano che hanno dovuto buttare via alcuni pacchetti per congestione allora questo viene di fatto creato un modello di messaggistica di controllo.

Questo modello viene utilizzato da host e router per scambiarsi informazioni. Il servizio, nonostante sia un servizio necessario al livello 4, però utilizza dei pacchetti di livello 3.

I messaggi ICMP sono piuttosto semplici: campo tipo, campo codice e un frammento di datagramma.

I messaggi ICMP più utilizzati sono:

<img src="image/image-20211214104609372.png" alt="image-20211214104609372" style="zoom:50%;" />

Poi ci sono una serie di messaggi che servono al client e che servono al router (ad esempio che il router non riesce a connettersi all’host). TTL scaduto se i pacchetti devono essere mollati. Errata intestazione IP serve per la mobilità, per cui serve un router particolare che supporta la versione mobile e in questo caso è necessario cercare il router quando c’è.

Traceroute funziona utilizzando i messaggi ICMP in questo modello:

<img src="image/image-20211214105028892.png" alt="image-20211214105028892" style="zoom:50%;" />

### IPv6

La versione 6 di IP è stata teorizzata nel 2000 per un esigenza che era già chiara. Di fatto utilizzare gli indirizzi a 32bit è un modello di indirizzi troppo restrittivo, nonostante all’inizio sembrassero troppi.

Di fatto si è creato un nuovo protocollo IP, ma siccome si aveva studiato molto il protocollo IPv4 allora si voleva inserire alcune modifiche per renderlo migliore:

* cercare di avere un formato con meno campi, in modo da migliorare la velocità del servizio

* frammentazione lato router non è veramente necessaria e fa solo perdere tempo

  

* Esigenza principale: il numero di indirizzi è troppo piccolo 

  * Adesso si hanno $2^{128}$ indirizzi, questo significa che potenzialmente il campo indirizzi può descrivere un numero di host infinito.

  * con questo modello quindi si risolvere il problema del numero di indirizzi

    

* il formato d’intestazione di questo nuovo modello rende più veloce la connessione e i processi di elaborazione e inoltro

L’intestazione della parte obbligatoria diventa 40byte e lunghezza fissa (il protocollo IP versione 4 ha lunghezza 20byte). Di fatto anche se il pacchetto è più grande le informazioni contenute sono minori, infatti ci sono meno campi contenuti in questo modello, ma si è data la priorità a contenere indirizzi molto vasti.



<img src="image/image-20211214105705587.png" alt="image-20211214105705587" style="zoom:50%;" />

L’intestazione è sempre a 32bit, ma ci sono meno campi siccome il *source address* e il *destination address* sono 4 righe per campo entrambe.

* campo versione: che indica la versione 6 del protocollo IP
* *priorità di flusso*: attribuisce una certa priorità a dei campi determinati di flusso

* *etichetta di flusso* (**flow label**) attribuisce priorità a determinati datagrammi di un flusso. Questo è un modello per indicare che una certa serie di pacchetti sono dello stesso flusso di dati e quindi non è necessario che il router faccia un elaborazione.
* *payload len*: lunghezza del payload, che indica la lunghezza le pacchetto
* *intestazione successiva* (**next hdr**): indica il protocollo a cui verranno consegnati i contenuti del datagramma
  * di fatto questo campo server a indicare delle eventuali parti opzionali (sempre ridotte al minimo) all’interno del pacchetto.



#### Rappresentazione di questi indirizzi

Solitamente questo indirizzo si scrive in esadecimale => impossibile ricordarsi a memoria gli indirizzi degli host. Quando si vedono i doppi punti allora vuol dire che c’è un gruppo di zeri.

```http
fe80::1400::81c6::a5f1::dd6c%en0
```

Di solito questo indirizzo si concatena con il velore dell’interfaccia (`en0`). 

Logicamente in questa versione del protocollo i dns e tutti questi servizi diventeranno necessari, siccome ricordare questo campo di indirizzi sarà praticamente impossibile.



#### Transizione da Version 4 a Versione6

Il problema è che la maggior parte dei router di accesso sono in versione 4 e non è possibile aggiornare allo stesso momento tutti i router. Di fatto ci sono alcuni servizi che non si possono fermare completamente per un certo tempo. Inoltre deve essere un operazione che deve essere fatte a passi.

Nonostante questo ad oggi molte reti sono alla versione 6, e le connessioni di alcuni paesi anche. 

In pratica per far coesistere le due versioni si utilizza il cosiddetto **tunneling**: si prende un pacchetto ad un certo livello e lo utilizza come payload di un livello che non è successivo. in questo modello se si fa tra due punti allora p come se si facesse passare una connessione dentro l’altra.

<img src="image/image-20211214111446403.png" alt="image-20211214111446403" style="zoom:50%;" />

Questo modello di fatto fa in modo di inserire un pacchetto Ipv6 all’interno di un pacchetto IPv4, oppure viceversa (un pacchetto IPv4 incapsulato dentro un IPv6).

La vista fisica e logica di questo modello è sviluppata in questo modo:

<img src="image/image-20211214111855428.png" alt="image-20211214111855428" style="zoom:50%;" />

Di fatto i router sul confine devono saper gestire entrambi i protocolli, in modo da poter istanziare il tunnel e permette l’inoltro in questo tunnel. Logicamente il funzionamento lavora anche nell’altra direzione.

Questa modalità permette di mantenere la versione 4 utilizzabile, mentre tutta la rete si aggiorna.



La rete IPv6 com’è messa? Secondo Google il 30% dei router di accesso supporta le connessioni IPv6, ovvero che la gente accede direttamente in IPv6. Si possono vedere statistiche aggiornate [qui](https://www.google.com/intl/en/ipv6/statistics.html).

La lentezza di questo cambiamento sono più di una:

* i servizi non sono facilissimi da applicare
* i router di bordo che devono instanziare il tunneling non sono molto economici



Di fatto però la completa conversione permetterebbe di avere una rete più uniforme e più reattiva.



## 4.5 Algoritmi di instradamento

Studiamo quali sono le procedure per trovare il percorso in una rete. Le capacità di inoltro sono sviluppare nel router per mandare i pacchetti da una porta all’altra.

Ci serve ancora conoscere come si fa a trovare una strada su internet. Di fatto ogni router ha una tabella di instradamento locale/inoltro che permetterà al router di inoltrare pacchetti.

Il compito dell’algoritmo di instradamento è il responsabile della popolazione delle tabelle di instradamento  locale. è importante trovare dei meccanismi che permettano di costruire queste tabelle anche per reti molto grandi.

Questo modello è basato su un algoritmo matematico che permette di trovare i percorsi, che di appoggia alla minimizzazione di una funzione. Poi dobbiamo trasformarlo in qualcosa che consenta ai nodi di parlarsi e di scambiarsi informazioni in maniera standard.

Il modo con cui la rete è collegata in un astrazione matematica e su cui poi si possono pensare gli algoritmi di instradamento è il grafo. In questo modello non andremo molto in profondità allo strumento matematico. Di fatto utilizziamo il grafo siccome utilizziamo la nozione di insiemi di nodi e insieme di archi che collegano gli host. 

Abbiamo i nodi della rete che vengono collegati, poi abbiamo gli archi che sono i collegamenti diretti tra i router, ovvero che hanno tra di loro un collegamento di livello 2 oppure di livello fisico.

Di fatto per capire il grafo è importante sapere cos’è un router e quali sono gli host poi. Di fatto poi attraverso questo metodo è possibile visualizzare bene la rete, poi non pone molto vincoli sulla rappresentazione della rete, ma permette anche di avere una serie di nodi in tutte le configurazioni.

Un esempio di grafo è:

![image-20211215095610546](image/image-20211215095610546.png)

<img src="image/image-20211215095632658.png" alt="image-20211215095632658" style="zoom:50%;" />

Da questo grafico si può capire il costo di collegamento per ogni ramo. Esempio: $c(w, z) = 5$. Generalmente non ci sono costi negativi nelle reti che affronteremo noi. Una volta definiti i costi dei collegamenti diretti, bisogna trovare un metodo per sommare i collegamenti. In questo modo riusciamo a calcolare i costi per andare da un nodo a un altro non direttamente collegato, allora sommiamo i costi dei singoli collegamenti. In base a costi dei collegamenti, la formula per trovare il collegamento migliore bisogna definire il modo per trovare il costo migliore.

Il funzionamento ad alto livello di questo modello è che: si trovano tutti i percorsi possibili, si trova il costo unitario di ogni percorso e si sceglie il percorso migliore selezionando quello con costo minore. Il costo del collegamento di solito è associato dal gestore della rete, che si occupa di assegnare un valore a ogni collegamento. Di solito se i costi sono tutti uguali l’algoritmo troverà il percorso con meno salti (siccome tutti costano uguale, cerco di minimizzare il numero di percorsi che devono essere fatti).

Quindi di solito si riesce a ottenere il funzionamento di questi algoritmi attraverso questi valori di costo assegnati a ogni collegamento. Di fatto più si vuole il collegamento inutilizzato, più si assegna un valore alto al collegamento, in questo modo verrà scelto solo poche volte.

Logicamente l’assegnazione dei costi deve essere fatta in modo coscienzioso, in questo modo si può dare la priorità a alcuni collegamenti migliori di altri, oppure si farà in modo che si preferisca un dato collegamento rispetto a un altro.

### Globale o Centralizzato, Statico o Dinamico

Ci sono due algoritmi di questo tipo, uno che ha necessità di avere tutte le informazioni prima (ha necessità di una visione globale), un altro modello permette di avere una conoscenza distribuita.

I modelli che funzionano a livello globale vengono chiamati **link-state algorithm** oppure **algoritmi a stato di collegamento**. Siccome questo modello è globale, quindi significa che per lanciare questo algoritmo è necessario conoscere tutte le informazioni dei collegamenti della rete.

Il modello a funzionamento decentralizzato sono chiamati **algoritmi a vettore distanza** oppure **DV, distance-vector algorithms**.  I costi di collegamento vengono fatti da ogni nodo in modo distribuito attraverso una stima del collegamento con i nodi vicini. In questo modo il cammino a costo minimo viene calcolato in modo distribuito e iterativo, in modo da trovare le situazioni migliori.

Poi di algoritmi poi ce ne sono tantissimi, migliori/peggiori in prestazioni. Poi le implementazione dei due modelli possono essere equivalenti nei costi, ma l’applicazione è diversa in base agli scopi che si vogliono raggiungere.



Un instradamento statico permette di fare calcoli su dei cammini che cambiano molto raramente. Questo significa che una volta che si elaborano i valori di instradamento allora rimangono costanti per un valore di tempo. Chiaramente questi algoritmi vengono ricalcolati una volta passato un certo valore di tempo, ma di fatto rimangono costanti sulla connessione.

Instradamento dinamico è applicato a delle reti che continuano a cambiare (ad esempio delle reti di veicoli). In questo modello la rete è sempre in un processo di modifica, quindi è necessario che gli algoritmi reagiscano molto frequentemente (anche, ad esempio, che la rete perda un qualche nodo che si disconnette). Altri tipi di reti di questo tipo possono reagire a un volume di traffico diverso o altri cambiamenti della topologia della rete. Questi valori sono costanti durante l’applicazione dell’algoritmo, siccome in caso contrario sarebbe difficile anche effettuare il calcolo. Spesso questa mobilità viene applicata alle reti di accesso che per fornire flessibilità agli host permetto un continuo cambio di valori degli host e delle posizioni degli host.

Questa mobilità non deve essere confusa con il modello di connessione in mobilità che, ad esempio, fornisce il wifi. Questa rete infatti è si senza fili e permette la mobilità degli host, ma di fatto ogni host connesso a un modello di rete di questo tipo ha una topologia costante.

Quando si parla di topologia della rete si parla di grafico. Ad esempio da stella diventa circolare oppure altre modifiche del genere.



### Algoritmo Dijkstra

Questo algoritmo è un algoritmo a **stato di collegamento**.

Per ottenere questo algoritmo è necessario che tutti i nodi conoscano i costi di ogni link. Questo stato dei dati si ottiene attravero un *link-state broadcast*, ovvero tutti i nodi comunicano ai vicini le informazioni dei collegamenti che possiedono, in modo che tutti i nodi abbiano la visione completa. Questa operazione è costoso dal punto computazionale e di banda, infatti si inonda la rete con un sacco di pacchetti di stato tra i nodi della rete, motivo per cui di solito è meglio lanciare questo algoritmo poche volte e soprattutto quando la rete è più scarica. Inoltre se la rete è stabile non è necessario continuare a lanciarlo.

Questo algoritmo funziona attraverso il calcolo svolto su un nodo di tutti i percorsi, quindi di fatto la spiegazione viene fatta da un unico nodo. Di base viene fatto un calcolo del costo di ogni collegamento da un nodo iniziale a tutti gli altri.

Una volta sviluppati questi algoritmi si salvano tutti i migliori collegamenti che serviranno poi a costruire la **tabella di inoltro**.

Questo algoritmo indica che se c’è un certo numero di nodi su questa rete, allora esiste un certo numero di direzioni. 

Questo algoritmo è lineare in funzione dei nodi della rete di cui dobbiamo calcolare il collegamento. Infatti la $k$-esima iterazione di questo algoritmo permette a $k$ nodi di conoscere i costi minimi delle sue connessioni.

Per spiegare questo algoritmo è necessario definire dei valori:

* $c(x, y)$: costo dei collegamenti dal nodo $x$ al nodo $y$. Questo costo è uguale a $\infin$ se i nodi non sono adiacenti, se no il valore è un numero intero.

* $D(v)$ costo del cammino dal nodo di origine alla destinazione $v$ rispetto all’iterazione dell’algoritmo corrente. 

  Questo costo varierà in base all’interazione, quindi finché l’algoritmo non è terminato si avranno una serie di valori parziali.

* $p(v)$: questo valore indica il predecessore del nodo di arrivo $v$.

* $N’$ è il sottoinsieme di $N$ (dove $N$ è l’insieme di tutti i nodi della rete su cui si sta lanciando l’algoritmo) per cui il cammino a costo minimo dall’origine è definitivamente noto.

  Infatti poi vedremo come ad ogni passo questo algoritmo sappia identificare un percorso che ha costo unitario minimo tra tutti i percorsi.



```pseudocode
// inizializzazione
// inserimento del nodo U nella rete
N_primo = {U}
// da questo momento si calcola il costo di ogni collegamento diretto
// indichiamo con V un nodo di U
while(V)
	if(V.isDirect(U)):
		D(V) = c(U, V);
	else:
		D(V) = INF;
		
// inizia il ciclo => Continua finché non si svolge il calcolo su tutti i nodi
while(N != N_primo) 
	// determina un nodo W, non in N_primo tale che D(W) sia minimo
	// aggiungi W a N_primo
	min = W_1;
	forEach(W)
		if(W < W_1):
			min = W
	N_primo.add(W);
	
	// Aggiorno D(v) per ogni nodo V adiacente a W e non in N_primo
	D(V) = min( D(V), D(W)+c(w, v));
    // questo significa che il *nuovo costo del percorso verso V* è:
    // - il vecchio percorso verso V
    // (oppure)
    // - il costo del cammino noto minimo (D(W)) sommato al costo del percorso da W a V

```

Un esempio di questa applicazione può essere:

![image-20211215115215084](image/image-20211215115215084.png)

Chiaramente il risultato sarà visualizzato in funzione del nodo che ha richiamato l’algoritmo, e in questo modo riesce a calcolare la via più veloce per raggiungere tutti gli host della rete, avendo come punto di partenza se stesso. L’algoritmo lanciato in $U$ infatti restituisce questi valori (che contribuiscono alla creazione della tabella di inoltro):

![image-20211215115859738](image/image-20211215115859738.png)







Una visualizzazione e spiegazione visuale dell’algoritmo in 3 minuti è visibile a [questo algoritmo](https://www.youtube.com/watch?v=_lHSawdgXpI).

La complessità di questo algoritmo, implementato nel modello più efficace possibile è $O(nlogn)$.



Si noti una cosa: in questo algoritmo si utilizzano i costi del collegamento come indicazione della quantità di traffico. Di base questa pratica non è *mai* consigliata siccome:

1. per definizione dell’algoritmo si sceglierà la sezione di rete sempre meno carica

2. la rete sarà carica in base alle scelte precendenti, quindi si otterrà un continuo cambiamento di routing rispetto all’istradamento iniziale, che potrebbe causare instabilità alla rete.

   ![image-20211215120554969](image/image-20211215120554969.png)

   Questo fatto non sarebbe un problema, se non fosse per l’RTT del collegamento così variabile. Infatti, soprattutto TCP (che ha necessità di fare un calcolo dell’RTT) potrebe avere dei problemi di stabilità e di funzionamento, siccome si otterrebbero dei valori teorici molto falsati che non rispecchiano sempre la stessa rete fisica e che quindi sono soggetti a più errori. Questo potrebbe causare eccessivi timeout, oppure un throughtput molto ridotto a causa di perdita di pacchetti frequenti.

### Algoritmo con vettore distanza

Questo algoritmo proviene dalla programmazione dinamica: si risolve un problema attraverso la scomposizione in problemi più piccoli e più facilmente risolvibili distribuiti su più agenti.

L’algoritmo che prevede questa applicazione si chiama **Bellman-Ford**. Per capire questo algoritmo è necessario sapere che $d_x(y)$ è il costo del percorso a costo minimo dal nodo $x$ al nodo $y$. Il secondo concetto è il calcolo di questo valore: $d_x(y) = min_v(c(x, v)+d_v(y))$.

Questo valore deve essere il minimo tra tutti i vicini di $y$.

Immaginare che tutti i router vicini informino il nodo richiedente del valore di risposta di un determinato nodo. In questo modo, attraverso questa formula voglio trovare il migliore tra i collegamenti, sommando il valore dei vari nodi, facendo però una richiesta a ogni pezzo della rete.

Questo algoritmo lavora in maniera distribuita, ovvero fa i calcoli per se stesso e poi propaga le informazioni. Piano piano ogni nodo conosco anche le informazioni per i nodi più lontani, in questo modo posso sapere anche le informazioni dei vicini. 

Questo funzionamento fa in modo da richiedere il valore del percorso minimo per un determinato tragitto ai vicini e poi sommare il proprio costo per raggiungere il vicino, infine minimizza.

Uno dei vantaggi di questo algoritmo funziona attraverso questo modello distribuito, facendo così ogni nodo calcola per se stesso e poi informa i vicini. Così facendo la potenza computazionale necessaria è minore e distribuita su tutta la rete, e a nessun nodo è richiesto informarsi su tutti i nodi.

In questo modo inoltre si diminuiscono anche i pacchetti da mandare sulla rete, infatti la rete non ha bisogno di mandare tutti i percorsi, ma solo i minimi. Inoltre questo scambio di pacchetti si ferma automaticamente quando tutti i nodi hanno ottenuto il valore minimo.



Con questo algoritmo vengono condivise solo le informazioni che permettono di avere la tratta minore. Inoltre i costi di questi circuiti vengono aggiornati (diminuendo il costo del collegamento) allora questa informazione probabilmente influenza un sacco di percorsi e viene condivisa con gli altri nodi velocemente, invece se una distanza vettoriale aumenta, allora più difficilmente i percorsi possono cambiare. Così facendo questo modello punta sempre a scegliere i costi migliori tra tutti i percorsi. L’unico modo per far viaggiare velocemente anche le informazioni negative sulla rete allora è necessario impostare un valore massimo della distanza di collegamento. In questo modo quando si raggiunge quel valore allora il percorso viene definito come $\infin$ e quindi tutto il percorso cade e viene annullato, portando a cercare un alternativa.



Link della visualizzazione di questo algoritmo a questo [link](https://www.youtube.com/watch?v=9PHkk0UavIM).



### Confronti tra i due algoritmi

Complessità dei messaggi:

* LS (Dijkstra): durante la fase bisogna mandare un numero di massaggi nel valore di $O(nE)$ ($n$ nodi e $E$ collegamenti). Questo valore è molto elevato (soprattutto se la rete è molto grande).

* DV (Bellman-Ford): siccome il tempo di convergenza è variabile questa fase può portare alla produzione di un numero di messaggi variabile e dipende dalla velocità con cui si propagano queste informazioni. 

  Questo significa anche che la situazione potrebbe essere più instabile per più tempo.

Velocità di convergenza:

* LS: l’algoritmo ha complessità $O(n^2)$ e richiedere di mandare $O(nE)$ messaggi, per cui ci possono essere oscillazioni di velocità.

  Non può presentare cicli di instradamento (quindi se un valore è circolare allora si otterrà un valore continuamente variabile fra due) e di fatto la rete non sarà mai stabile.

* DV: la convergenza può avvenire molto più lentamente e può presentare cicli di instradamento.

  Può presentare il problema del conteggio dell’infinito, ovvero non dando un upperbound allora è difficile che la peggiore strada possibile venga identificata e eliminata causando una modifica dei percorsi istantanea.

Robustezza: (cosa succede se un router funziona male)

* LS: se il router funziona male allora il router rappresenta un errore solo per se stesso, infatti tutti i calcoli per tutti i nodi vengono fatti in locale, creando un problema di instradamento sbagliato solo sul suo nodo.
* DV: in teoria il nodo che fornisce informazioni sbagliate potrebbe portare all’indirizzamento sbagliato di molti nodi della rete, siccome la sua informazione sarebbe ridistribuita su tutta la rete.



### Instradamento Gerarchico

Il problema degli algoritmi di instradamento è che sono molto dispendiosi sulle reti molto grandi, motivo per cui è necessario trovare un modello che ne riduca la complessità.

Inoltre si avrebbe un problema di archiviazione, infatti non si potrebbero immagazzinare tutti i dati di tutti gli host su ogni nodo della rete siccome sarebbero troppe informazioni.

Questo modello viene trovato con il concetto di internet per definizione, ovvero che la rete è una rete di reti.

Esiste quindi una serie di algoritmi che permettono di fare queste operazioni su delle isole interne. Poi esistono una serie di router che conoscono i metodi di implementazione per interfacciarsu sulla rete esterna e che quindi hanno necessità di conoscere tutti gli algoritmi di routing.

Ogni isola in questo schema si chiama *sistema autonomo (AS - **autonomus system**)* e permette di avere delle organizzazioni interne e esterne alla rete. Di solito questa entità eseguono lo stesso algoritmo di instradamento, che infatti vengono divisi tra **intra-AS** (protocolli di instradamento all’interno del sistema autonomo). Router appartenenti a diversi AS possono eseguire protocolli di instradamento intra-AS diversi (potenzialmente si possono scrivere dei protocolli di instradamento completamente proprietari, se si vuole ottenere un determinato obiettivo). 

**Inter-AS** sono i protocolli di instradamento *tra* i sistemi autonomi permettono di collegare i sistemi autonomi. Questi router, chiamati **router gateway** hanno la possibilità di far girare internamente il sistema di instradamento interno, all’esterno la IETF ha ideato un protocollo che permette di collegare le diverse isole di traffico, chiamato **PGP**.

Di fatto questo algoritmo permette di capire tra quali isole di traffico far passare il pacchetto in modo che sia il più veloce possibile, calcolo che è rimandato all’algoritmo inter.



#### Protocolli IGP (Inter Gateway Protocol)

Questi protocolli sono conosciuti anche come intra-AS. Servono quindi a gestire l’instradamento del traffico interno a una rete.

I protocolli intra-AS più comuni sono:

* **RIP**: routing informazion protocol
* **OSPF**: open shortest path first
* **IGRP**: interior gateway routing protocol



##### RIP

è un protocollo a vettore distanza. Utilizza come metrica il numero di hop che l’host deve fare, e inoltre è di default su UNIX BSD dal 1982 (quindi piuttosto antico).

Ogni collegamento costano uguale (1) e quindi i collegamenti diretti saranno solo $1$ hop. Il massimo numero di hop contabili è 15, quindi è impostato l’infinito a 16 salti. In questo modo non si ha mai un collegamento che fa più di 15 salti di connessioni tra i nodi.

In questo protocollo i nodi si scambiano un messaggio di risposta RIP (RIP advertisement, oppure messaggio di annuncio RIP). Questo pacchetto indica le distante relative ad un massimo di 25 sottoreti di destinazione all’interno del sistema autonomo.

Guasto di comunicazione e advertisement: quando il nodo adiacente non risponde per un intervallo di 180 secondi (ovvero 6advertisement) allora il nodo adiacente o il collegamento vengono ritenuti spenti o guasti, e di fatto si ricalcola la tabella di routing locale. Dopodichè propaga l’informazione mandando annunci ai router vicini, che mandano nuovi messaggi se la loro tabella d’instradamento è cambiata a sua volta. Di fatto l’informazione di propagazione di un router guasto si propaga velocemente su tutta la rete. Si può utilizzare anche la tecnica dell’**inversione avvelenata**, ovvero si indica quel collegamento come numero di hop infinito (16hop), in questo modo questo percorso non può essere scelto da nessuno e si procede al ricalcolo di tutti i percorsi.

Questo calcolo viene fatto da un processo, chiamato **routed** all’interno del sistema operativo UNIX. Questo processo è quello che implementa RIP. Questo è un processo particolare che ha i privilegi per scrivere nelle tabelle di inoltro (cosa particolare, siccome sono solitamente riservati a livello tre questi privilegi). Questo modello esce leggermente dal contesto del 3livello, ma permette di avere una comunicazione su dei pacchetti UDP standard. Questa soluzione è abbastanza interessante, siccome non necessita di nessun altro protocollo di comunicazione, ma si appoggia a questo già esistente.



##### OSPF

