# Lezione Reti 8 Novembre



![image-20211108155941875](image/image-20211108155941875.png)

Quando il mittente ricevesse gli ACK dei primi segmenti allora torna ad essere allineato. 



Non in ordine ma già riscontrato vuol dire che quando è stato ricevuto ma erano fuori sequenza e quindi li ho messi nel buffer. 



[animazione](https://www2.tkn.tu-berlin.de/teaching/rn/animations/gbn_sr/)





In teoria la ripetizione selettiva è quella che offre le performance maggiori. 

Molti sistemi ad ora utilizzano la ripetizione selettiva, ma tutte le versioni supportano la versione cumulativa. Poi in realtà su questi protocolli si dichiarano alla prima connessione con il server.

TCP sono finestre di 32bit (4 milioni ?) (anche se non numera i segmenti ma fa un altro calcolo). Il numero delle finestre invece è un parametro che cambia in base alla connessione, come il time-out che viene calcolato alla prima connessione, siccome ci sono diversi delay sulla connessione.

Nella realtà dimensione della finestra e del timeout quindi sono dei parametri modificabili.

